//
//  Owner.swift
//  CarsAndOwners
//
//  Created by Yaroslav Zhurbilo on 10.07.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import Foundation

// This is a class to present an Owner item
class Owner {
    
    private var _dbID: String!       // ID of the owner in the Database
    private var _firstName: String!  // First name of the Owner
    private var _lastName: String!   // Last name of the Owner
    
    //TODO: Add licence number
    
    private var _image: String!      // Image of the Owner
    private var _carsOwned: [Car]!   // The list of the cars owned by this Person
    
    func matchSearchBarText(_ searchBarText: String) -> Bool {
        let text = searchBarText.lowercased()
        if firstName.lowercased().contains(text) { return true }
        if lastName.lowercased().contains(text) { return true }
        
        return false
    }
    
    // Full name of the Person
    var fullName: String {
        return firstName + " " + lastName
    }
    
    // Getter for the inner variable _firstName
    var dbID: String {
        
        get {
            
            if _dbID != nil {
                return _dbID
            } else {
                return ""
            }
            
        }
    }
    
    // Getter for the inner variable _firstName
    var firstName: String {
        
        get {
            
            if _firstName != nil {
                return _firstName
            } else {
                return ""
            }
            
        }
    }
    
    // Getter for the inner variable _lastName
    var lastName: String {
        
        get {
            
            if _lastName != nil {
                return _lastName
            } else {
                return ""
            }
            
        }
    }
    
    // Getter for the inner variable _image
    var image: String {
        
        get {
            
            if _image != nil {
                return _image
            } else {
                return ""
            }
            
        }
    }
    
    // Getter for the inner variable _carsOwned
    var carsOwned: [Car] {
        
        get {
            
            if _carsOwned != nil {
                return _carsOwned
            } else {
                return []
            }
            
        }
    }
    
    // Designated initializer
    init(dbID: String, firstName: String, lastName: String, image: String, cars: [Car]) {
        
        self._dbID = dbID
        _firstName = firstName
        _lastName = lastName
        _image = image
        _carsOwned = cars
        
    }
    
    convenience init(){
        
        self.init(dbID: "", firstName: "", lastName: "", image: "", cars: [])
        
    }
    
    convenience init(ownerID: String, firebaseDict: [String:Any]) {
        
        // Check Data before initialize
        var saveFirstName = ""
        var saveLastName = ""
        var saveImage = ""
        var saveCars: [Car] = []
        
        if let firstName = firebaseDict["firstName"] as? String {
            saveFirstName = firstName
        }
        
        if let lastName = firebaseDict["lastName"] as? String {
            saveLastName = lastName
        }
        
        if let image = firebaseDict["image"] as? String {
            saveImage = image
        }
        
        if let cars = firebaseDict["cars"] as? [Car] {
            saveCars = cars
        }
        
        // Initialize designated initializer
        self.init(dbID: ownerID,
                  firstName: saveFirstName,
                  lastName: saveLastName,
                  image: saveImage,
                  cars: saveCars)
    }
}
















