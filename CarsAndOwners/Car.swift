//
//  Car.swift
//  CarsAndOwners
//
//  Created by Yaroslav Zhurbilo on 10.07.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import Foundation

// This is a class to present a Car item
class Car {
    
    private var _dbID: String!      // ID of this car in the Database
    private var _brand: String!     // Brand name of the car
    private var _model: String!     // Model name of the car
    private var _owner: String!     // Owner's ID
    private var _image: String!     // Image of the car
    private var _number: String!    // The number of car
    private var _color: String!     // Color of the car
    private var _mileAge: String!   // Mileage of the car
    private var _bodyNumber: String!         // The number of car body
    private var _passportNumber: String!     // The number of tech passport
    private var _yearOfManufacture: String!  // Year of made
    
    func matchSearchBarText(_ searchBarText: String) -> Bool {
        let text = searchBarText.lowercased()
        if brand.lowercased().contains(text) { return true }
        if model.lowercased().contains(text) { return true }
        if owner.lowercased().contains(text) { return true }
        if number.lowercased().contains(text) { return true }
        if color.lowercased().contains(text) { return true }
        if mileAge.lowercased().contains(text) { return true }
        if bodyNumber.lowercased().contains(text) { return true }
        if passportNumber.lowercased().contains(text) { return true }
        if yearOfManufacture.lowercased().contains(text) { return true }
        
        return false
    }
    
    // Full description of the car in the list
    var description: String {
        var description = yearOfManufacture + " " + brand + " " + model + " " + color
        if mileAge != "" {
            description += " (" + mileAge + " miles)"
        }
        return  description
    }
    
    // Getter for the inner variable _model
    var dbID: String {
        
        get {
            if _dbID != nil {
                return _dbID
            } else {
                return ""
            }
        }
        
    }
    
    // Getter for the inner variable _model
    var brand: String {
        
        get {
            if _brand != nil {
                return _brand
            } else {
                return ""
            }
        }
        
    }
    
    // Getter for the inner variable _model
    var model: String {
        
        get {
            if _model != nil {
                return _model
            } else {
                return ""
            }
        }
        
    }
    
    // Getter for the inner variable _owner
    var owner: String {
        
        get {
            if _owner != nil {
                return _owner
            } else {
                return ""
            }
        }
        
    }
    
    // Getter for the inner variable _image
    var image: String {
        
        get {
            if _image != nil {
                return _image
            } else {
                return ""
            }
        }
        
    }
    
    // Getter for the inner variable _color
    var number: String {
        
        get {
            if _number != nil {
                return _number
            } else {
                return ""
            }
        }
        
    }
    
    // Getter for the inner variable _color
    var color: String {
        
        get {
            if _color != nil {
                return _color
            } else {
                return ""
            }
        }
        
    }
    
    // Getter for the inner variable _mileAge
    var mileAge: String {
        
        get {
            if _mileAge != nil {
                return _mileAge
            } else {
                return ""
            }
        }
        
    }
    
    // Getter for the inner variable _mileAge
    var bodyNumber: String {
        
        get {
            if _bodyNumber != nil {
                return _bodyNumber
            } else {
                return ""
            }
        }
        
    }
    
    // Getter for the inner variable _mileAge
    var passportNumber: String {
        
        get {
            if _passportNumber != nil {
                return _passportNumber
            } else {
                return ""
            }
        }
        
    }
    
    // Getter for the inner variable _made
    var yearOfManufacture: String {
        
        get {
            if _yearOfManufacture != nil {
                return _yearOfManufacture
            } else {
                return ""
            }
        }
        
    }
    
    // Designated initializer
    init(dbID: String, brand: String, model: String,
         owner: String, image: String,
         number: String, color: String,
         mileAge: String, bodyNumber: String,
         passportNumber: String, yearOfManufacture: String) {
        
        // Begin to init variables
        self._dbID = dbID
        self._brand = brand
        self._model = model
        self._owner = owner
        self._image = image
        self._number = number
        self._color = color
        self._mileAge = mileAge
        self._bodyNumber = bodyNumber
        self._passportNumber = passportNumber
        self._yearOfManufacture = yearOfManufacture
    }
    
    convenience init(carID: String, firebaseDict: [String:Any]) {
        
        // Check Data before initialize
        var saveBrand = ""
        var saveModel = ""
        var saveOwner = ""
        var saveImage = ""
        var saveNumber = ""
        var saveColor = ""
        var saveMileAge = ""
        var saveBodyNumber = ""
        var savePassportNumber = ""
        var saveYearOfManufacture = ""
        
        // Assign previous variables with values
        
        if let brand = firebaseDict["brand"] as? String {
            saveBrand = brand
        }
        
        if let model = firebaseDict["model"] as? String {
            saveModel = model
        }
        
        if let owner = firebaseDict["owner"] as? String {
            saveOwner = owner
        }
        
        if let image = firebaseDict["image"] as? String {
            saveImage = image
        }
        
        if let number = firebaseDict["number"] as? String {
            saveNumber = number
        }
        
        if let color = firebaseDict["color"] as? String {
            saveColor = color
        }
        
        if let mileAge = firebaseDict["mileAge"] as? String {
            saveMileAge = mileAge
        }
        
        if let bodyNumber = firebaseDict["bodyNumber"] as? String {
            saveBodyNumber = bodyNumber
        }
        
        if let passportNumber = firebaseDict["passportNumber"] as? String {
            savePassportNumber = passportNumber
        }
        
        if let yearOfManufacture = firebaseDict["yearOfManufacture"] as? String {
            saveYearOfManufacture = yearOfManufacture
        }
        
        // Initialize designated initializer
        self.init(dbID: carID, brand: saveBrand,
                  model: saveModel, owner: saveOwner,
                  image: saveImage, number: saveNumber,
                  color: saveColor, mileAge: saveMileAge,
                  bodyNumber: saveBodyNumber, passportNumber: savePassportNumber,
                  yearOfManufacture: saveYearOfManufacture)
        
    }
    
}










