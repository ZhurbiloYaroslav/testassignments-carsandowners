//
//  OwnerCell.swift
//  CarsAndOwners
//
//  Created by Yaroslav Zhurbilo on 10.07.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit

// This is a custom cell for Owner in the Owners list
class OwnerCell: UITableViewCell {

    @IBOutlet weak var ownerFullName: UILabel!
    @IBOutlet weak var ownerCarsQuantity: UILabel!
    
    // Configure the Cell in the table View
    func configureCell(owner: Owner) {
        
        self.ownerFullName.text = owner.fullName
        self.ownerCarsQuantity.text = "\(owner.carsOwned.count)"
    }

}
