//
//  CarOwnersVC.swift
//  CarsAndOwners
//
//  Created by Yaroslav Zhurbilo on 10.07.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit
import Foundation
import FirebaseDatabase
import FirebaseAuth

class CarOwnersVC: UIViewController {
    
    @IBOutlet weak var tableViewWithOwnersList: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var ref: DatabaseReference!  // Reference variable for the Database
    var handle: AuthStateDidChangeListenerHandle! // Variable to handle Firebase Listener
    
    var ownersList: [Owner]!
    var searchBarQueryText: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialise delegates
        initialiseDelegates()
        
        // Configure the Firebase
        configureTheFirebase()
        
        // Set the observer for Firebase data
        setTheObserverForFirebaseData()
        
    }
    
    // Set the observer for Firebase data
    func setTheObserverForFirebaseData() {
        
        ref.observe(.value, with: { snapshot in
            self.updateTable(snapshot)
        })
        
    }
    
    // Configure the Firebase
    func configureTheFirebase() {
        
        let firebaseConnect = FirebaseConnect()
        firebaseConnect.configureDatabase()
        self.ref = firebaseConnect.ref
        self.handle = firebaseConnect.handle
        
    }
    
    func initialiseDelegates() {
        
        tableViewWithOwnersList.delegate = self
        tableViewWithOwnersList.dataSource = self
        
        searchBar.delegate = self
    }
    
    // Update the table with owners list at the begining and after changes in Firebase
    func updateTable(_ snapshot: DataSnapshot) {
        
        // Define variable with the whole data from Database
        guard let snapshotValue = snapshot.value as? [String: Any] else {
            return
        }
        
        // Define variable with only the list of owners
        guard let ownersListFromFirebase = snapshotValue["owners"] as? [String: [String: Any]] else {
            return
        }
        
        // Make a temporary variable for storing owners list from Firebase
        var updatedOwnersList = [Owner]()
        
        // Make objects of the owners list from Firebase and put them in the array
        for (key, value) in ownersListFromFirebase {
            let owner = Owner(ownerID: key, firebaseDict: value)
            updatedOwnersList.append(owner)
        }
        
        // Update this class variable with data from Firebase
        ownersList = updatedOwnersList
        tableViewWithOwnersList.reloadData()
    }
    
    // Returns the list of filtered owners
    func returnTheListOfOwners() -> [Owner]? {
        
        if let searchQuery = searchBarQueryText, searchQuery != "" {
            
            // If searchQuery is not empty, then we return the filtered list
            var filteredListOfOwners = [Owner]()
            
            for owner in ownersList {
                // Append new item to the list if it's fields contains search text
                
                if owner.matchSearchBarText(searchQuery) {
                    filteredListOfOwners.append(owner)
                }
            }
            
            return filteredListOfOwners
            
        } else if ownersList != nil {
            
            // If searchQuery is empty, then we return the whole list of owners
            return ownersList
            
        } else {
            
            // If the list of owners is empty
            return nil
            
        }
    }
    
    // Add data to Database
    func addInitialDataAtTheFirstLunct() {
        
        //TODO: Add initial data at the first lunch
        
    }
    
    // Send the owner object to the Edit page
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditCarOwner" {
            if let destination = segue.destination as? CarOwnerAddEditVC {
                if let owner = sender as? Owner {
                    destination.ownerToEdit = owner
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Remove Firebase Authenticate listener
        if let tempHandle = handle {
            Auth.auth().removeStateDidChangeListener(tempHandle)
        }
        
    }
    
    // This function handles unwind from the next view controllers to current
    @IBAction func unwindToOwnersListWithNoSave(segue: UIStoryboardSegue) {}
    
}

// Extend our ViewController with TableView and related methods
extension CarOwnersVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let listOfOwners = returnTheListOfOwners(), listOfOwners.count > 0 {
            return listOfOwners.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CarOwnerCell") as? OwnerCell else {
            return UITableViewCell()
        }
        if let filteredListOfOwners = returnTheListOfOwners() {
            cell.configureCell(owner: filteredListOfOwners[indexPath.row])
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        // Delete row
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") {
            (action, indexPath) in
            
            let currentOwnerID = self.ownersList[indexPath.row].dbID
            self.ref.child("owners").child(currentOwnerID).removeValue()
        }
        
        return [deleteAction]
    }
    
    // Go to edit owner in the Add/Edit view controller
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "EditCarOwner", sender: ownersList[indexPath.row])
    }
}

// Implement methods for Search Bar Controller
extension CarOwnersVC: UISearchBarDelegate {
    
    // Filter list of items using searchBar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchBarQueryText = searchText
        tableViewWithOwnersList.reloadData()
        
        if let listOfOwners = returnTheListOfOwners() {
            print(listOfOwners)
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
}










