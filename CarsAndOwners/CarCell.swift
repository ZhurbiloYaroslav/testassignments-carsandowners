//
//  CarCell.swift
//  CarsAndOwners
//
//  Created by Yaroslav Zhurbilo on 10.07.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit

// This is a custom cell for Car item in the Cars list
class CarCell: UITableViewCell {

    @IBOutlet weak var fullDescription: UILabel!
    @IBOutlet weak var ownerFullName: UILabel!
    
    // Configure the Cell in the table View
    func configureCell(car: Car) {
        
        self.fullDescription.text = car.description
        self.ownerFullName.text = car.owner
        
    }

}
