//
//  CarAddEditVC.swift
//  CarsAndOwners
//
//  Created by Yaroslav Zhurbilo on 10.07.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class CarAddEditVC: UIViewController {
    
    var ref: DatabaseReference!  // Reference variable for the Database
    var handle: AuthStateDidChangeListenerHandle! // Variable to handle Firebase Listener
    
    // Variable to receive an object of car by Segue from CarsListVC
    var carToEdit: Car?
    
    @IBOutlet weak var carBrand: UITextField!
    @IBOutlet weak var carModel: UITextField!
    @IBOutlet weak var carOwner: UITextField!
    @IBOutlet weak var carNumber: UITextField!
    @IBOutlet weak var carMileage: UITextField!
    @IBOutlet weak var carBodyNumber: UITextField!
    @IBOutlet weak var carPassportNumber: UITextField!
    @IBOutlet weak var carYearOfManufacture: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure the Firebase
        configureTheFirebase()
        
        // Update fields and View if we came here after press the table cell from the cars list
        updateViewWhenEditing()
        
    }
    
    // Configure the Firebase
    func configureTheFirebase() {
        
        let firebaseConnect = FirebaseConnect()
        firebaseConnect.configureDatabase()
        self.ref = firebaseConnect.ref
        self.handle = firebaseConnect.handle
        
    }
    
    // Update fields and View if we came here after press the table cell from the cars list
    func updateViewWhenEditing() {
        guard let car = carToEdit else { return }
        
        // Update Fields for editing the Car
        carBrand.text = car.brand
        carModel.text = car.model
        carOwner.text = car.owner
        carNumber.text = car.number
        carMileage.text = "\(car.mileAge)"
        carBodyNumber.text = car.bodyNumber
        carPassportNumber.text = car.passportNumber
        carYearOfManufacture.text = car.yearOfManufacture
        
        //TODO: init fields
        
        navigationItem.title = "Edit"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tempHandle = handle {
            Auth.auth().removeStateDidChangeListener(tempHandle)
        }
        
    }
    
    // Perform an Action when press on save button while Add or Edit a car item
    @IBAction func carSaveButtonPressed(_ sender: UIBarButtonItem) {
        
        // Get the dictionary with values from text fields
        guard let carValuesDict = makeDictionaryWithFieldsValues() else { return }
        
        // Inserting data to Firebase
        if let car = carToEdit {
            // Update current owner
            ref.child("cars").child(car.dbID).updateChildValues(carValuesDict)
        } else {
            // Add new car
            ref.child("cars").childByAutoId().setValue(carValuesDict)
        }
        
        // Dismiss this view controller and go to previous
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func makeDictionaryWithFieldsValues() -> Dictionary<String, Any>? {
        
        //Check to have a correct results
        guard let brand = carBrand.text else { return nil }
        guard let model = carModel.text else { return nil }
        guard let owner = carOwner.text else { return nil }
        guard let number = carNumber.text else { return nil }
        guard let mileAge = carMileage.text else { return nil }
        guard let bodyNumber = carBodyNumber.text else { return nil }
        guard let passportNumber = carPassportNumber.text else { return nil }
        guard let yearOfManufacture = carYearOfManufacture.text else { return nil }
        
        //TODO: Make a check for required fields
        
        // Make a adictionary with values for inserting to Firebase
        let carValuesDict = [
            "brand": brand,
            "model": model,
            "owner": owner,
            "number": number,
            "mileAge": mileAge,
            "bodyNumber": bodyNumber,
            "passportNumber": passportNumber,
            "yearOfManufacture": yearOfManufacture
        ]
        
        return carValuesDict
    }
    
}





