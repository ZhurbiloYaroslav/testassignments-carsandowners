//
//  CarsListVC.swift
//  CarsAndOwners
//
//  Created by Yaroslav Zhurbilo on 10.07.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit
import Foundation
import FirebaseDatabase
import FirebaseAuth

class CarsListVC: UIViewController {
    
    @IBOutlet weak var tableViewWithCarsList: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var ref: DatabaseReference!  // Reference variable for the Database
    var handle: AuthStateDidChangeListenerHandle! // Variable to handle Firebase Listener
    
    var carsList: [Car]!
    var searchBarQueryText: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialise delegates
        initialiseDelegates()
        
        // Configure the Firebase
        configureTheFirebase()
        
        // Set the observer for Firebase data
        setTheObserverForFirebaseData()
        
    }
    
    // Set the observer for Firebase data
    func setTheObserverForFirebaseData() {
        
        ref.observe(.value, with: { snapshot in
            self.updateTable(snapshot)
        })
        
    }
    
    // Configure the Firebase
    func configureTheFirebase() {
        
        let firebaseConnect = FirebaseConnect()
        firebaseConnect.configureDatabase()
        self.ref = firebaseConnect.ref
        self.handle = firebaseConnect.handle
        
    }
    
    func initialiseDelegates() {
        
        tableViewWithCarsList.delegate = self
        tableViewWithCarsList.dataSource = self
        
        searchBar.delegate = self
    }
    
    // Update the table with cars list at the begining and after changes in Firebase
    func updateTable(_ snapshot: DataSnapshot) {
        
        // Define variable with the whole data from Database
        guard let snapshotValue = snapshot.value as? [String: Any] else {
            return
        }
        
        // Define variable with only the list of cars
        guard let carsListFromFirebase = snapshotValue["cars"] as? [String: [String: Any]] else {
            return
        }
        
        // Make a temporary variable for storing cars list from Firebase
        var updatedCarsList = [Car]()
        
        // Make objects of the cars list from Firebase and put them in the array
        for (key, value) in carsListFromFirebase {
            let car = Car(carID: key, firebaseDict: value)
            updatedCarsList.append(car)
        }
        
        // Update this class variable with data from Firebase
        carsList = updatedCarsList
        tableViewWithCarsList.reloadData()
    }
    
    // Returns the list of filtered cars
    func returnTheListOfCars() -> [Car]? {
        
        if let searchQuery = searchBarQueryText, carsList != nil, searchQuery != "" {
            
            // If searchQuery is not empty, then we return the filtered list
            var filteredListOfCars = [Car]()
            
            for car in carsList {
                // Append new item to the list if it's fields contains search text
                if car.matchSearchBarText(searchQuery) {
                    filteredListOfCars.append(car)
                }
            }
            
            return filteredListOfCars
            
        } else if carsList != nil {
            return carsList // If searchQuery is empty, then we return the whole list of owners
        } else {
            return nil // If the list of owners is empty
        }
    }
    
    // Send the car object to the Edit page
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditCar" {
            if let destination = segue.destination as? CarAddEditVC {
                if let car = sender as? Car {
                    destination.carToEdit = car
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Remove Firebase Authenticate listener
        if let tempHandle = handle {
            Auth.auth().removeStateDidChangeListener(tempHandle)
        }
        
    }
    
    // This function handles unwind from the next view controllers to current
    @IBAction func unwindToCarsListWithNoSave(segue: UIStoryboardSegue) {}

}

// Extend our ViewController with TableView and related methods
extension CarsListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let listOfCars = returnTheListOfCars(), listOfCars.count > 0 {
            return listOfCars.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CarCell") as? CarCell else {
            return UITableViewCell()
        }
        if let filteredListOfCars = returnTheListOfCars() {
            cell.configureCell(car: filteredListOfCars[indexPath.row])
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        // Delete row
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") {
            (action, indexPath) in
            
            let currentCarID = self.carsList[indexPath.row].dbID
            self.ref.child("cars").child(currentCarID).removeValue()
        }
        
        return [deleteAction]
    }
    
    // Go to edit car in the Add/Edit view controller
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "EditCar", sender: carsList[indexPath.row])
    }
    
}

// Implement methods for Search Bar Controller
extension CarsListVC: UISearchBarDelegate {
    
    // Filter list of items using searchBar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchBarQueryText = searchText
        tableViewWithCarsList.reloadData()
        
        if let listOfOwners = returnTheListOfCars() {
            print(listOfOwners)
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
}











