//
//  CarOwnerAddEditVC.swift
//  CarsAndOwners
//
//  Created by Yaroslav Zhurbilo on 10.07.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class CarOwnerAddEditVC: UIViewController {
    
    var ref: DatabaseReference!  // Reference variable for the Database
    var handle: AuthStateDidChangeListenerHandle! // Variable to handle Firebase Listener
    
    // Variable to receive an object of owner by Segue from CarOwnersVC
    var ownerToEdit: Owner?
    
    @IBOutlet weak var ownerFirstName: UITextField!
    @IBOutlet weak var ownerLastName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure the Firebase
        configureTheFirebase()
        
        // Update fields and View if we came here after press the table cell from the owners list
        updateViewWhenEditing()
        
    }
    
    // Configure the Firebase
    func configureTheFirebase() {
        
        let firebaseConnect = FirebaseConnect()
        firebaseConnect.configureDatabase()
        self.ref = firebaseConnect.ref
        self.handle = firebaseConnect.handle
        
    }
    
    // Update fields and View if we came here after press the table cell from the owners list
    func updateViewWhenEditing() {
        guard let owner = ownerToEdit else { return }
        ownerFirstName.text = owner.firstName
        ownerLastName.text = owner.lastName
        
        navigationItem.title = "Edit"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tempHandle = handle {
            Auth.auth().removeStateDidChangeListener(tempHandle)
        }
        
    }
    
    // Perform an Action when press on Save button while Add or save Owner item
    @IBAction func ownerSaveButtonPressed(_ sender: Any) {
        
        // Get the dictionary with values from text fields
        guard let ownerValuesDict = makeDictionaryWithFieldsValues() else { return }
    
        // Inserting data to Firebase
        if let owner = ownerToEdit {
            // Update current owner
            ref.child("owners").child(owner.dbID).updateChildValues(ownerValuesDict)
        } else {
            // Add new owner
            ref.child("owners").childByAutoId().setValue(ownerValuesDict)
        }
        
        
        // Dismiss this view controller and go to previous
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    //
    func makeDictionaryWithFieldsValues() -> Dictionary<String, Any>? {
        
        //TODO: Make a check to have a correct results
        guard let firstName = ownerFirstName.text else { return nil }
        guard let lastName = ownerLastName.text else { return nil }
        
        // Make a adictionary with values for inserting to Firebase
        
        let ownerValuesDict = [
            "firstName": firstName,
            "lastName": lastName
        ]
        
        return ownerValuesDict
    }
    
}



